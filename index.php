<!DOCTYPE html>
<html>

<head>
	<?php include "Snippet/meta.php"?>
	<?php include "Snippet/bootstrap.php"?>

	<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
	<link rel="stylesheet" href="css/generic.css">

	<script src="js/nav.js"></script>

	<link rel="shortcut icon" href="https://png.icons8.com/ultraviolet/50/000000/bot.png" type="image/x-icon">
	<title>Bot php</title>
</head>

<body id="wrapper" class="animate">
	
	<?php include "Template/Header.php"?>
		
	<section class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title"><img class="ico" src="img/KingsAge.ico"/> KingsAge</h5>
						<h6 class="card-subtitle mb-2 text-muted">Bot para "KingsAge"</h6>
						<p class="card-text">© 2009 por Gameforge 4D GmbH.</p>
						<a href="./kingsAge.php" class="card-link">Go to Bot of "KingsAge"</a>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title"><img class="ico" src="img/elBruto.ico"> El Bruto</h5>
						<h6 class="card-subtitle mb-2 text-muted">Bot para "El Bruto"</h6>
						<p class="card-text">© 2009 por Motion Twin.</p>
						<a href="./elBruto.php" class="card-link">Go to Bot of "El Bruto"</a>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card">
					<div class="card-body">
						
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card">
					<div class="card-body">
						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Try Other</h5>
						<h6 class="card-subtitle mb-2 text-muted">Template</h6>
						<p class="card-text">Template to long card.</p>
						<a href="#" class="card-link">link</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>

</html>