
<script src="js/header.js"></script>
<link rel="stylesheet" href="css/header.css">
<header>
	<nav class="navbar header-top fixed-top navbar-expand-lg  navbar-dark bg-dark">
		<span class="navbar-toggler-icon leftmenutrigger"></span>
		<a class="navbar-brand no-padding no-margin" href="./index.php"><img id="logo" src="https://png.icons8.com/ultraviolet/50/000000/bot.png"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav animate side-nav">
				<li class="nav-item">
					<a class="nav-link" href="./index.php">Home
						<span class="sr-only">(current)</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="./kingsAge.php"><img class="ico" src="img/KingsAge.ico"/> KingsAge</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="./elBruto.php"><img class="ico" src="img/elBruto.ico"/> El Bruto</a>
				</li>
			</ul>
	<!--	<ul class="navbar-nav ml-md-auto d-md-flex">
				<li class="nav-item">
					<a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Top Menu Items</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">Pricing</a>
				</li>
			</ul> -->
		</div>
	</nav>
</header>