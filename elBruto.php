<!DOCTYPE html>
<html>

<head>
	<?php include "Snippet/meta.php"?>
	<?php include "Snippet/bootstrap.php"?>

	<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
	<link rel="stylesheet" href="css/generic.css">

	<script src="js/nav.js"></script>

	<link rel="shortcut icon" href="https://png.icons8.com/ultraviolet/50/000000/bot.png" type="image/x-icon">
	<title>Bot El Bruto</title>
</head>

<body id="wrapper" class="animate">
	
	<?php include "Template/Header.php"?>
		
	<section class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Copiar la sesión:  </h5>
						<textarea name="sessionElBruto" class="form-control"></textarea>
						<button type="submit" class="btn btn-success">Guardar la sesión</button>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Try Other</h5>
						<h6 class="card-subtitle mb-2 text-muted">Template</h6>
						<p class="card-text">Template to long card.</p>
						<a href="#" class="card-link">link</a>
					</div>
				</div>
			</div>
		</div>
	</section>

</body>

</html>