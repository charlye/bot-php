<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>404 - Not Found</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" type="text/css" media="screen" href="css/404.css" />
    <script src="js/404.js"></script>
</head>
<body>
    <section class="error_section">
        <p class="error_section_subtitle">Opps Page is not available !</p>
        <h1 class="error_title">
            <p>404</p>
            404
        </h1>
        <a href="#" class="btn">Back to home</a>
        </section>
</body>
</html>